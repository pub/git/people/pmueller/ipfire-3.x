###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = rrdtool
version    = 1.8.0
release    = 1.2

groups     = Applications/Databases
url        = https://oss.oetiker.ch/rrdtool/
license    = GPLv2+ with exceptions
summary    = Round Robin Database Tool to store and display time-series data

description
	RRD is the Acronym for Round Robin Database. RRD is a system to
	store and display time-series data. It stores the data in a
	very compact way that will not expand over time, and it presents
	useful graphs by processing the data to enforce a certain data
	density.
end

source_dl  = https://github.com/oetiker/rrdtool-1.x/releases/download/v%{version}/

build
	requires
		cairo-devel
		freetype-devel
		gcc-c++
		gettext
		groff
		libtool
		libxml2-devel
		openssl-devel
		pango-devel
		pkg-config
		python3-devel
		python3-setuptools
		systemd-devel
		zlib-devel
	end

	configure_options += \
		--disable-rrdcgi \
		--disable-perl \
		--disable-tcl \
		--disable-ruby \
		--disable-static \
		--enable-python \
		--disable-rrdcached \
		--with-pic \
		PYTHON=%{python3}

	prepare_cmds
		# Remove rpath
		sed -e "s/--rpath=.* &/\&/" -i bindings/Makefile.*
	end

	install_cmds
		# Remove unused files
		rm -rvf %{BUILDROOT}/usr/share/rrdtool/examples

		# Remove locale dir because no locales were installed
		rm -rvf %{BUILDROOT}/usr/share/locale
	end
end

packages
	package %{name}
		requires = %{name}-libs = %{thisver}
	end

	package %{name}-libs
		template LIBS
	end

	package python3-%{name}
		template PYTHON3
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
