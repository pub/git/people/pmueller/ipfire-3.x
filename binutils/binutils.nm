###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = binutils
version    = 2.40
release    = 1.1

maintainer = Michael Tremer <michael.tremer@ipfire.org>
groups     = Development/Tools
url        = https://www.gnu.org/software/binutils/
license    = GPLv2+
summary    = The GNU Binutils are a collection of binary tools

description
	The GNU Binary Utilities, or binutils, is a collection of programming
	tools for the manipulation of object code in various object file formats.
end

source_dl = https://ftp.gnu.org/gnu/binutils/ \
	https://sourceware.org/pub/binutils/releases/
sources   = %{thisapp}.tar.xz

build
	requires
		bison
		dejagnu
		flex
		gcc-c++
		perl
		texinfo
		zlib-devel
		zstd-devel
	end

	LDFLAGS += -Wl,--enable-new-dtags

	prepare_cmds
		mkdir -pv %{DIR_SRC}/binutils-build
	end

	configure_options += \
		--build=%{DISTRO_BUILDTARGET} \
		--host=%{DISTRO_BUILDTARGET} \
		--target=%{DISTRO_BUILDTARGET} \
		--disable-werror \
		--disable-static \
		--enable-shared \
		--enable-64-bit-bfd \
		--enable-plugins \
		--with-bugurl="https://bugzilla.ipfire.org/" \
		--enable-relro=yes \
		--enable-ld \
		--disable-gold \
		--enable-lto \
		--with-system-zlib \
		--enable-new-dtags \
		--disable-rpath \
		--enable-separate-code

	build
		cd %{DIR_SRC}/binutils-build
		../%{thisapp}/configure \
			%{configure_options}

		make tooldir=/usr %{PARALLELISMFLAGS}
	end

	test
		cd %{DIR_SRC}/binutils-build
		make -k check </dev/null || :

		echo "==== RESULTS ===="
		cat {gas/testsuite/gas,ld/ld,binutils/binutils}.sum
	end

	install
		cd %{DIR_SRC}/binutils-build
		make tooldir=/usr install DESTDIR=%{BUILDROOT}

		cp -fv %{DIR_APP}/include/libiberty.h %{BUILDROOT}/usr/include

		# Prevent packages from linking against libbfd and libopcodes,
		# because they change too often.
		rm -rfv %{BUILDROOT}%{libdir}/lib{bfd,opcodes}.so

		# Remove Windows/Novell only man pages.
		rm -vf %{BUILDROOT}%{mandir}/man1/{dlltool,nlmconv,windres}*
	end
end

packages
	package %{name}

	package %{name}-libs
		template LIBS

		files += %{libdir}/*-%{version}.so
	end

	package %{name}-devel
		template DEVEL

		requires
			binutils-libs = %{thisver}
			zlib-devel
		end

		files += !%{libdir}/*-%{version}.so
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
