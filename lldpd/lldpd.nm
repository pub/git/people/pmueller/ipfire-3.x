###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = lldpd
version    = 1.0.16
release    = 2.1

groups     = Networking/Tools
url        = https://lldpd.github.io
license    = ISC
summary    = Utilities for the Link Layer Discovery Protocol

description
	The LLDPD project aims to provide a comprehensive implementation of
	the IEEE standard 802.1AB Link Layer Discovery Protocol.
end

source_dl = https://media.luffy.cx/files/lldpd/

build
	requires
		libcap-devel
		libevent-devel
		(libseccomp-devel if arch(x86_64))
		libxml2-devel
		pkg-config
		readline-devel
		shadow-utils
	end

	configure_options += \
		--without-embedded-libevent \
		--with-readline \
		--with-xml \
		--with-privsep-user=lldpd \
		--with-privsep-group=lldpd

	# lldpd does not support seccomp on aarch64.
	if "%{DISTRO_ARCH}" == "aarch64"
		configure_options += --without-seccomp
	else
		configure_options += --with-seccomp
	end

	prepare_cmds
		%{create_user}
	end
end

create_user
	getent group lldpd >/dev/null || groupadd -r lldpd
	getent passwd lldpd >/dev/null || \
		useradd -r -g lldpd -d / -s /sbin/nologin lldpd
end

packages
	package %{name}
		prerequires
			shadow-utils
			systemd-units
		end

		script prein
			%{create_user}
		end

		script postin
			/bin/systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script preun
			/bin/systemctl --no-reload disable lldpd.service >/dev/null 2>&1 || :
			/bin/systemctl stop lldpd.service >/dev/null 2>&1 || :
		end

		script postun
			/bin/systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			/bin/systemctl daemon-reload >/dev/null 2>&1 || :
			/bin/systemctl try-restart lldpd.service >/dev/null 2>&1 || :
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
