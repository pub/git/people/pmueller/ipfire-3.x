###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = systemd
version    = 254
release    = 3

maintainer = Stefan Schantl <stefan.schantl@ipfire.org>
groups     = System/Base
url        = http://www.freedesktop.org/wiki/Software/systemd
license    = GPLv2+
summary    = A System and Service Manager

description
	systemd is a system and service manager for Linux, compatible with
	SysV and LSB init scripts.
end

source_dl  = https://github.com/%{name}/%{name}/archive/v%{version}.tar.gz#/

build
	unitdir = %{prefix}/lib/systemd/system

	# The tools which are shipped by systemd requires an rpath to
	# the libsystemd-shared binary to run properly. So the macro for
	# removing any rpath's have to be disabled.
	MACRO_FIX_LIBTOOL = # Do nothing

	requires
		# Testsuite
		/usr/bin/getfacl
		/usr/bin/mount
		/usr/bin/tree
		/usr/bin/umount

		attr-devel
		cryptsetup-luks-devel >= 1.4.2
		dbus-devel
		docbook-utils
		docbook-xsl
		elfutils-devel
		gettext-devel
		glib2-devel
		gobject-introspection-devel >= 1.31
		gperf
		hwdata
		intltool >= 0.51.0
		kbd
		kmod-devel >= 15
		libacl-devel
		libbpf-devel
		libblkid-devel
		libcap-devel
		libcurl-devel
		libfdisk-devel
		libgcrypt-devel
		libidn-devel
		libidn2-devel
		libmount-devel >= 2.34-2
		libpwquality-devel
		libseccomp-devel
		libuuid-devel >= 2.34-2
		libxcrypt-devel
		lz4-devel
		m4
		meson >= 0.50.1
		ninja >= 1.9.0
		openssl-devel
		p11-kit-devel
		pam-devel
		pciutils-devel
		python3-devel
		python3-jinja2
		python3-lxml
		setup >= 3.0-13
		usbutils
		vala
		xz-devel
		zlib-devel
	end

	export LD_LIBRARY_PATH = %{DIR_APP}/.libs

	configure_options = \
		-Dsysvinit-path= \
		-Dsysvrcnd-path= \
		-Dnobody-user=nobody \
		-Dnobody-group=nobody \
		-Dfallback-hostname="localhost" \
		-Dzlib=true \
		-Dman=true \
		-Dsmack=false \
		-Dbacklight=false \
		-Dfirstboot=false \
		-Dhibernate=false \
		-Dimportd=false \
		-Dlibiptc=false \
		-Dmachined=false \
		-Dnetworkd=false \
		-Dresolve=false \
		-Dportabled=false \
		-Dtimesyncd=false \
		-Dtimedated=false \
		-Dpolkit=false

	# Assign group ids
	configure_options += \
		-Dadm-gid=4 \
		-Dtty-gid=5 \
		-Ddisk-gid=6 \
		-Dlp-gid=7 \
		-Dkmem-gid=9 \
		-Dwheel-gid=10 \
		-Dcdrom-gid=11 \
		-Ddialout-gid=18 \
		-Dutmp-gid=22 \
		-Dtape-gid=33 \
		-Dkvm-gid=36 \
		-Dvideo-gid=39 \
		-Daudio-gid=63 \
		-Dusers-gid=100 \
		-Dinput-gid=104 \
		-Drender-gid=105 \
		-Dsgx-gid=106 \
		-Dsystemd-journal-gid=190

	build
		# Call meson and pass configure options.
		%{meson} %{configure_options}

		# Call ninja to start build
		%{meson_build}
	end

	test
		# Create dummy machine-id file, which is required for some tests.
		touch %{sysconfdir}/machine-id

		# Run the testsuite.
		%{meson_test} || true
	end

	install
		# Install systemd.
		%{meson_install}

		# Use the installed files to create all users and groups
		# in one shot.
		%{create_groups}

		# Create folder in log to store the journal.
		mkdir -pv %{BUILDROOT}/var/log/journal

		# Create sysv compatible symlinks.
		mkdir -pv %{BUILDROOT}%{sbindir}
		ln -svf ../lib/systemd/systemd  %{BUILDROOT}/%{sbindir}/init
		ln -svf ../lib/systemd/systemd  %{BUILDROOT}/%{bindir}/systemd
		ln -svf ../bin/systemctl %{BUILDROOT}/%{sbindir}/reboot
		ln -svf ../bin/systemctl %{BUILDROOT}/%{sbindir}/halt
		ln -svf ../bin/systemctl %{BUILDROOT}/%{sbindir}/poweroff
		ln -svf ../bin/systemctl %{BUILDROOT}/%{sbindir}/shutdown

		# Create empty machine-id file.
		touch %{BUILDROOT}/etc/machine-id

		# Copy locale and console settings
		mkdir -pv %{BUILDROOT}%{sysconfdir}
		cp -vf %{DIR_SOURCE}/{locale,vconsole}.conf %{BUILDROOT}%{sysconfdir}

		# Recreate all targets
		rm -rfv %{BUILDROOT}/%{sysconfdir}/systemd/system/*.target.wants
		for i in basic.target.wants default.target.wants dbus.target.wants getty.target.wants \
			multi-user.target.wants syslog.target.wants; do
				mkdir -pv %{BUILDROOT}/%{sysconfdir}/systemd/system/${i} || exit 1
		done

		# Remove runlevel targets and graphical.target
		rm -rfv %{BUILDROOT}%{unitdir}/runlevel*
		rm -rfv %{BUILDROOT}%{unitdir}/graphical.target

		# Remove service files for utmp update.
		rm -rvf %{BUILDROOT}%{unitdir}/systemd-update-utmp-*.service
		rm -rvf %{BUILDROOT}%{unitdir}/shutdown.target.wants/systemd-update-utmp-*.service

		# Set default target to multi-user
		ln -svf multi-user.target %{BUILDROOT}%{unitdir}/default.target

		# Replace absolute symlinks by relative ones.
		cd %{BUILDROOT}%{unitdir}/../user
		for i in bluetooth local-fs paths printer remote-fs shutdown smartcard sockets sound swap timers; do
			ln -svf ../system/${i}.target ${i}.target || exit 1
		done

		# Remove .so file for the shared library, it's not supposed to be used for anything.
		rm -rvf %{BUILDROOT}%{prefix}/lib/systemd/libsystemd-shared.so

		# Remove tmpfile for X11
		rm -rfv %{BUILDROOT}/usr/lib/tmpfiles.d/x11.conf

		# Drop X11 related config files
		rm -rvf %{BUILDROOT}%{sysconfdir}/X11

		# Remove unneeded stuff for xdg
		rm -rfv %{BUILDROOT}/etc/xdg

		# Remove unneeded stuff for rpm.
		rm -rvf %{BUILDROOT}/etc/rpm
		rm -rvf %{BUILDROOT}/usr/lib/rpm

		# Drop ZSH related files
		rm -rvf %{BUILDROOT}%{datadir}/zsh

		# Use journald. Enable the import of kernel messages.
		# Stop forwarding to a syslog daemon.
		sed \
			-e "s/^#ForwardToSyslog=.*$/ForwardToSyslog=no/" \
			-e "s/^#ImportKernel=.*$/ImportKernel=yes/" \
			-i %{BUILDROOT}/etc/systemd/journald.conf

		# Remove udev rules for "predictable" network device renaming.
		# http://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/
		rm -vf %{BUILDROOT}/usr/lib/udev/rules.d/80-net-name-slot.rules

		# Don't let udev set up any networking
		rm -vf %{BUILDROOT}/usr/lib/udev/rules.d/80-net-setup-link.rules

		mkdir -pv %{BUILDROOT}/var/log/journal
		chown root:systemd-journal %{BUILDROOT}/var/log/journal

		# Create empty environment file.
		touch %{BUILDROOT}%{sysconfdir}/environment
	end
end

create_groups
	# Create the sysusers folder in the jail.
	mkdir -pv %{sysusersdir}

	# Copy all sysusers files from BUILDROOT into the jail's sysusers directory.
	install -v -m 644 %{BUILDROOT}%{sysusersdir}/*.conf %{sysusersdir}

	# We do not have a libsystemd or systemd-sysusers binary, so
	# call the installed one inside BUILDROOT to create all users and
	# groups in one shot.
	LD_LIBRARY_PATH="%{BUILDROOT}%{libdir}/systemd/" \
		%{BUILDROOT}%{bindir}/systemd-sysusers
end

packages
	package %{name}
		requires
			dbus
			hwdata
			python3-cairo
			python3-dbus
			python3-gobject3
			%{name}-basetools = %{thisver}
			%{name}-libs = %{thisver}
			util-linux >= 2.19
		end

		provides
			/bin/systemctl
			/bin/systemd
			/sbin/halt
			/sbin/init
			/sbin/poweroff
			/sbin/reboot
			/sbin/shutdown

			udev = %{thisver}
			systemd-units = %{thisver}

			syslog
		end

		conflicts
			bash-completion < 2.1
			dracut < 019
			filesystem < 002
			rsyslog < 5.8.6-4
			setup < 3.0-13
			upstart
		end

		obsoletes
			udev < 183
			systemd-units <= 242
		end

		configfiles
			/etc/locale.conf
			/etc/machine-id
			/etc/systemd/journald.conf
			/etc/systemd/logind.conf
			/etc/systemd/system.conf
			/etc/systemd/user.conf
			/etc/udev/udev.conf
			/etc/vconsole.conf
		end

		script postin
			# Reexec systemd daemon.
			/usr/bin/systemctl daemon-reexec > /dev/null 2>&1 || :

			# Automatically spawn a getty on TTY1
			/usr/bin/systemctl enable getty@.service >/dev/null 2>&1 || :

			# Enable targed to mount all remote filesystems.
			/usr/bin/systemctl enable remote-fs.target >/dev/null 2>&1 || :
		end

		script preup
			# Be sure to stop the old udev before updating.
			/usr/bin/systemctl stop udev.service udev-trigger.service \
				udev-control.socket udev-kernel.socket >/dev/null 2>&1 || :

			%{create_groups}
		end

		script postup
			# Re-exec systemd after update.
			/usr/bin/systemctl daemon-reexec >/dev/null 2>&1 || :

			# Restart login service after update
			/usr/bin/systemctl daemon-reload >/dev/null 2>&1 || :
			/usr/bin/systemctl try-restart systemd-logind.service >/dev/null 2>&1 || :

			# Use the new journald configuration file and restart the service.
			if ! grep -xq ImportKernel=yes /etc/systemd/journald.conf ; then
				mv /etc/systemd/journald.conf.paknew /etc/systemd/journald.conf >/dev/null 2>&1 || :
				/usr/bin/systemctl restart systemd-journald.service >/dev/null 2>&1 || :
			fi
		end

		# Be sure to start the new udev after everything is done.
		script posttransup
			/usr/bin/systemctl start systemd-udev.service  >/dev/null 2>&1 || :
		end
	end

	package %{name}-basetools
		summary = Basetools from the systemd package, like sysuses and tmpfiles.
		description
			This package contains some very basic tools from systemd like
			systemd-sysusers and systemd-tmpfiles.

			They mostly are required in a very early stage and shipping them
			in an own package allows us to handle this properly.
		end

		provides
			systemd-sysusers = %{thisver}
			systemd-tmpfiles = %{thisver}
		end

		requires
			%{name}-libs = %{thisver}
		end

		files
			%{bindir}/%{name}-sysusers
			%{bindir}/%{name}-tmpfiles
			%{mandir}/man1/%{name}-sysusers*
			%{mandir}/man1/%{name}-tmpfiles*

			# Only ship the sysusers.d folder without
			# any config files.
			%{sysusersdir}
			!%{sysusersdir}/*.conf
		end
	end

	package %{name}-libs
		template LIBS
	
		groups += Base

		prerequires
			/etc/nsswitch.conf
		end

		provides
			nss-myhostname = %{thisver}
		end

		obsoletes
			nss-myhostname <= 0.3-3
			systemd-compat-libs < 230
		end

		conflicts
			# Ensure that the version of systemd matches systemd-libs.
			systemd > %{thisver}
			systemd < %{thisver}
		end

		files += \
			%{libdir}/%{name}

		# Add myhostname to the hosts line of /etc/nsswitch.conf
		script postin
			if [ -f "/etc/nsswitch.conf" ]; then
				sed -i.bak -e '
					/^hosts:/ !b
					/\<myhostname\>/ b
					s/[[:blank:]]*$/ myhostname/
					' /etc/nsswitch.conf
			fi
		end

		# Remove myhostname from the hosts line of /etc/nsswitch.conf
		script postun
			if [ -f "/etc/nsswitch.conf" ]; then
				sed -i.bak -e '
					/^hosts:/ !b
					s/[[:blank:]]\+myhostname\>//
					' /etc/nsswitch.conf
			fi
		end
	end

	package %{name}-devel
		template DEVEL

		files += %{prefix}/lib/pakfire/macros/
	end

	package libudev
		summary = Libraries for adding libudev support to applications.
		description
			This package contains the libraries that make it easier to use libudev
			functionality from applications.
		end
		license = LGPLv2+

		conflicts
			filesystem < 002
		end

		files
			%{libdir}/lib*udev*.so.*
			%{libidr}/girepository*/
		end
	end

	package libudev-devel
		summary = Header files for adding libudev support to applications.
		description
			This package contains the header and pkg-config files for developing
			applications using libudev functionality.
		end
		license = LGPLv2+

		conflicts
			filesystem < 002
		end

		files
			%{libdir}/lib*udev*.so
			%{libdir}/pkgconfig/lib*udev.pc
			%{libdir}/pkgconfig/gudev*
			%{includedir}/lib*udev.h
			%{includedir}/gudev*
			%{datadir}/gir-*
			%{datadir}/gtk-doc/html/gudev
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
