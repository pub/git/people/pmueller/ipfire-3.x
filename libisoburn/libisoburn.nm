###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = libisoburn
version    = 1.5.4
release    = 1

groups     = System/Filesystems
url        = https://libburnia-project.org/
license    = GPLv2+ and LGPLv2+
summary    = Library to enable creation and expansion of ISO-9660 filesystems

description
	Libisoburn is a front-end for libraries libburn and libisofs which
	enables creation and expansion of ISO-9660 filesystems on all CD/
	DVD/BD media supported by libburn. This includes media like DVD+RW,
	which do not support multi-session management on media level and
	even plain disk files or block devices. Price for that is thorough
	specialization on data files in ISO-9660 filesystem images. And so
	libisoburn is not suitable for audio (CD-DA) or any other CD layout
	which does not entirely consist of ISO-9660 sessions.
end

source_dl = http://files.libburnia-project.org/releases/

build
	requires
		libacl-devel
		libattr-devel
		libburn-devel
		libisofs-devel
		zlib-devel
	end
end

packages
	package %{name}

	package xorriso
		summary = ISO-9660 and Rock Ridge image manipulation tool

		description
			Xorriso is a program which copies file objects from POSIX compliant
			filesystems into Rock Ridge enhanced ISO-9660 filesystems and allows
			session-wise manipulation of such filesystems. It can load management
			information of existing ISO images and it writes the session results
			to optical media or to filesystem objects. Vice versa xorriso is able
			to copy file objects out of ISO-9660 filesystems.
		end

		files
			%{bindir}/osirrox
			%{bindir}/xorrecord
			%{bindir}/xorriso*
			%{mandir}/man*/osirrox*
			%{mandir}/man*/xorrecord*
			%{mandir}/man*/xorriso*
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
