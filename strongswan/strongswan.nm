###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = strongswan
version    = 5.9.11
release    = 1

groups     = Networking/VPN
url        = https://www.strongswan.org/
license    = GPL
summary    = IPsec (IKEv1 + IKEv2) implementation for Linux

description
	StrongSwan is a complete IPsec and IKEv1 implementation for
	Linux 2.4 and 2.6 kernels. It also fully supports the new IKEv2
	protocol with Linux 2.6 kernels. It interoperates in both IKEv1
	and IKEv2 mode with most other IPsec-based VPN products.
end

source_dl  = https://download.strongswan.org/

build
	requires
		autoconf
		automake
		bison
		flex
		gmp-devel
		gperf
		ldns-devel
		libcap-devel
		libcurl-devel
		libgcrypt-devel
		libtool
		openldap-devel
		openssl-devel
		perl
		sqlite-devel
		systemd-devel >= 221-2
		unbound-devel >= 1.7
	end

	# strongswan cannot be compiled with -Wformat-security
	# because of some custom printf stuff
	CFLAGS += -Wno-format-security -Wno-error=format-security

	configure_options += \
		--disable-static \
		--with-ipseclibdir=%{libdir}/ipsec \
		--enable-curl \
		--enable-ldap \
		--enable-openssl \
		--enable-sqlite \
		--enable-gcrypt \
		--enable-ccm \
		--enable-ctr \
		--enable-gcm \
		--enable-chapoly \
		--enable-unity \
		--enable-xauth-eap \
		--enable-xauth-noauth \
		--enable-eap-radius \
		--enable-eap-tls \
		--enable-eap-ttls \
		--enable-eap-peap \
		--enable-eap-mschapv2 \
		--enable-eap-identity \
		--disable-blowfish \
		--disable-rc2 \
		--with-capabilities=libcap \
		--enable-unbound \
		--enable-systemd \
		--disable-charon \
		--disable-stroke \
		--disable-warnings

	test
		LD_LIBRARY_PATH="%{DIR_APP}/src/libstrongswan/.libs" make check || :
	end

	install_cmds
		# Install network settings
		install -v -m 644 %{DIR_SOURCE}/network.conf \
			%{BUILDROOT}%{sysconfdir}/strongswan.d/

		# Create directory for connections
		mkdir -pv %{BUILDROOT}%{sysconfdir}/swanctl/connections
		echo "include connections/*.conf" > \
			%{BUILDROOT}%{sysconfdir}/swanctl/swanctl.conf
	end
end

packages
	package %{name}
		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload >/dev/null 2>&1 || :
			systemctl reload-or-try-restart strongswan.service >/dev/null 2>&1 || :
		end

		script preun
			systemctl disable --no-reload strongswan.service >/dev/null 2>&1 || :
			systemctl stop strongswan.service >/dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
