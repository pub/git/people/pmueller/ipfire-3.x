###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = iptraf-ng
version    = 1.2.1
release    = 3

groups     = Networking/Tools
url        = https://github.com/iptraf-ng/iptraf-ng
license    = GPLv2+
summary    = A console-based network monitoring utility.

description
	IPTraf-ng is a console-based network monitoring utility.  IPTraf gathers
	data like TCP connection packet and byte counts, interface statistics
	and activity indicators, TCP/UDP traffic breakdowns, and LAN station
	packet and byte counts.  IPTraf-ng features include an IP traffic monitor
	which shows TCP flag information, packet and byte counts, ICMP
	details, OSPF packet types, and oversized IP packet warnings;
	interface statistics showing IP, TCP, UDP, ICMP, non-IP and other IP
	packet counts, IP checksum errors, interface activity and packet size
	counts; a TCP and UDP service monitor showing counts of incoming and
	outgoing packets for common TCP and UDP application ports, a LAN
	statistics module that discovers active hosts and displays statistics
	about their activity; TCP, UDP and other protocol display filters so
	you can view just the traffic you want; logging; support for Ethernet,
	FDDI, ISDN, SLIP, PPP, and loopback interfaces; and utilization of the
	built-in raw socket interface of the Linux kernel, so it can be used
	on a wide variety of supported network cards.
end

source_dl  = https://github.com/iptraf-ng/iptraf-ng/archive/refs/tags/v%{version}.tar.gz#/

build
	requires
		ncurses-devel
	end

	make_build_targets += \
		CFLAGS="%{CFLAGS}" \
		LDFLAGS="%{LDFLAGS}"

	make_install_targets += \
		prefix=%{prefix}

	install_cmds
		mkdir -pv %{BUILDROOT}%{localstatedir}/{log,lib}/iptraf-ng
	end
end

packages
	package %{name}
		# Create an alias for iptraf.
		provides = iptraf
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
