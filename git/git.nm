###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = git
version    = 2.38.1
release    = 5

groups     = Development/Tools
url        = https://git-scm.com/
license    = GPLv2
summary    = Fast Version Control System.

description
	Git is a fast, scalable, distributed revision control system with an
	unusually rich command set that provides both high-level operations
	and full access to internals.
end

source_dl  = https://www.kernel.org/pub/software/scm/git/

build
	requires
		asciidoc
		expat-devel
		gettext
		gnupg2
		groff
		libcurl-devel
		openssl-devel
		pcre2-devel
		perl-ExtUtils-MakeMaker
		perl-devel
		python3-devel
		xmlto
		zlib-devel
	end

	prepare_cmds
		cat <<EOF > config.mak
		CFLAGS = %{CFLAGS}
		LDFLAGS = %{LDFLAGS}
		NEEDS_CRYPTO_WITH_SSL = 1
		USE_LIBPCRE2 = 1
		ETC_GITCONFIG = %{sysconfdir}/gitconfig
		DESTDIR = %{BUILDROOT}
		INSTALL = install -p
		GNU_ROFF = 1
		NO_CROSS_DIRECTORY_HARDLINKS = 1
		PYTHON_PATH = %{python3}
		prefix = %{prefix}
		gitexecdir = lib/git-core
		htmldir = %{datadir}/doc
		NO_GITWEB=1
		EOF

		# Remove shebang from bash-completion script.
		sed -i '/^#!bash/,+1 d' contrib/completion/git-completion.bash
	end

	build
		make %{PARALLELISMFLAGS}
	end

	make_install_targets += INSTALLDIRS=vendor

	install_cmds
		# Install man pages.
		make install-doc DESTDIR=%{BUILDROOT}

		# Install bash completion file.
		mkdir -pv %{BUILDROOT}%{sysconfdir}/bash_completion.d
		install -v -m 644 contrib/completion/git-completion.bash \
			%{BUILDROOT}%{sysconfdir}/bash_completion.d/git

		# Remove git-daemon related files
		rm -rvf %{BUILDROOT}/usr/lib/git-core/git-daemon
		rm -rvf %{BUILDROOT}%{mandir}/man1/git-daemon*

		# Remove SVN related files
		rm -rvf %{BUILDROOT}/usr/lib/git-core/git-svn
		rm -rvf %{BUILDROOT}%{mandir}/man1/git-svn*
		rm -rvf %{BUILDROOT}%{perl_vendorlib}/Git/SVN*.pm
		rm -rvf %{BUILDROOT}%{perl_vendorlib}/Git/SVN/
		rm -rvf %{BUILDROOT}%{datadir}/perl5/Git/SVN/
		rm -rvf %{BUILDROOT}%{datadir}/perl5/Git/SVN*.pm

		# Remove CVS related files
		rm -rvf %{BUILDROOT}%{bindir}/git-cvsserver
		rm -rvf %{BUILDROOT}/usr/lib/git-core/*cvs*
		rm -rvf %{BUILDROOT}%{mandir}/man1/*cvs*
		rm -rvf %{BUILDROOT}%{mandir}/man7/*cvs*

		# Remove Perforce depots related files.
		rm -rvf %{BUILDROOT}/usr/lib/git-core/*p4*
		rm -rvf %{BUILDROOT}/usr/lib/git-core/mergetools/p4*
		rm -rvf %{BUILDROOT}%{mandir}/man1/*p4*

		# Fix file permissions of the hook examples.
		find %{BUILDROOT}%{datadir}/git-core/ -type f -iname "*.sample" \
			-exec chmod 644 {} \;
	end
end

packages
	package %{name}
		requires
			less
			openssh-clients
			/usr/bin/vi
		end

		recommends
			%{name}-email
			rsync
		end
	end

	package %{name}-email
		summary = Git tools for sending emails.
		description = %{summary}

		requires
			git=%{thisver}
			perl-Git=%{thisver}
			perl(Authen::SASL)
			perl(Net::SMTP::SSL)
		end

		files
			/usr/lib/git-core/git-send-email
			%{mandir}/man1/git-send-email*
		end
	end

	package perl-Git
		arch = noarch

		summary = Perl interface to Git.
		description = %{summary}

		requires
			git=%{thisver}
		end

		files
			%{mandir}/man3/Git*
			%{perl_archlib}
			%{perl_vendorlib}/*.pm
			%{perl_vendorlib}/Git/*.pm
			%{datadir}/perl5/
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
