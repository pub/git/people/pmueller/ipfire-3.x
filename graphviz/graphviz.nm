###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = graphviz
version    = 7.0.4
release    = 2.1

groups     = Development/Tools
url        = https://gitlab.com/graphviz/graphviz
license    = EPL
summary    = Graph Visualization Tools

description
	Graph visualization is a way of representing structural information
	as diagrams of abstract graphs and networks. It has important
	applications in networking, bioinformatics, software engineering,
	database and web design, machine learning, and in visual interfaces
	for other technical domains.
end

source_dl  = https://gitlab.com/graphviz/graphviz/-/archive/%{version}/
sources    = %{thisapp}.tar.bz2

build
	requires
		autoconf
		automake
		bison
		cairo-devel
		flex
		fontconfig-devel
		ghostscript-devel
		libjpeg-devel
		libpng-devel
		libtool
		pango-devel
	end

	prepare_cmds
		./autogen.sh
	end

	configure_options += \
		--enable-debug

	configure_cmds
		# Add some additional C compiler flags to proper harden liblab_gamut.
		sed -i '/^CFLAGS =/ s/$/ -fno-builtin-exit -D__noreturn__=/' \
			lib/edgepaint/Makefile

		# Add some additional C and C++ compiler flags to proper harden
		# the "dot" binaries.
		sed -i '/^CFLAGS =/ s/$/ -fno-builtin-exit -D__noreturn__=/' \
			cmd/dot/Makefile
		sed -i '/^CXXFLAGS =/ s/$/ -fno-builtin-exit -D__noreturn__=/' \
			cmd/dot/Makefile

		# Add some additional C compiler flags to proper harden the
		# "gvpr" binaries.
		sed -i '/^CFLAGS =/ s/$/ -fno-builtin-exit -D__noreturn__=/' \
			cmd/gvpr/Makefile

		# Add some additional C compiler flags to proper harden the
		# tools.
		sed -i '/^CFLAGS =/ s/$/ -fno-builtin-exit -D__noreturn__=/' \
			cmd/tools/Makefile
	end

	test
		make check
	end
end

packages
	package %{name}

	package %{name}-libs
		template LIBS
	end

	package %{name}-devel
		template DEVEL

		requires
			graphviz = %{thisver}
			graphviz-libs = %{thisver}
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
