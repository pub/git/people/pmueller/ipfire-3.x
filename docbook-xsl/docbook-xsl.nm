###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = docbook-xsl
version    = 1.79.2
release    = 3

groups     = Applications/Text
url        = https://docbook.sourceforge.net/projects/xsl/
license    = GPLv2+
summary    = Norman Walsh's XSL stylesheets for DocBook XML.

description
	hese XSL stylesheets allow you to transform any DocBook XML document to
	other formats, such as HTML, FO, and XHMTL.  They are highly customizable.
end

source_dl = https://github.com/docbook/xslt10-stylesheets/releases/download/release/%{version}/

sources = %{thisapp}.tar.bz2

build
	arches = noarch

	build
		# Nothing to do.
	end

	install
		# Create directory layout.
		mkdir -pv %{BUILDROOT}/%{datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}

		# Install catalog files.
		cp -avf [[:lower:]]* %{BUILDROOT}/%{datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}/
		cp -avf VERSION %{BUILDROOT}/%{datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}/VERSION.xsl

		ln -svf VERSION.xsl \
			%{BUILDROOT}/%{datadir}/sgml/docbook/xsl-ns-stylesheets-%{version}/VERSION
		ln -svf xsl-ns-stylesheets-%{version} \
			%{BUILDROOT}/%{datadir}/sgml/docbook/xsl-ns-stylesheets

		# Don't ship install shell script.
		rm -rvf %{BUILDROOT}/%{datadir}/sgml/docbook/xsl-ns-stylesheets/install.sh

		# Remove buggy extensions.
		rm -rfv %{BUILDROOT}/%{datadir}/sgml/docbook/xsl-stylesheets-%{version}/extensions/*

		# Remove unneeded tool for .epub formats.
		rm -rvf %{BUILDROOT}/%{datadir}/sgml/docbook/xsl-stylesheets-%{version}/epub

		# Fix directory permissions.
		find %{BUILDROOT}%{datadir} -type d -exec chmod 755 {} \;

		# Fix file permissions.
		find %{BUILDROOT}%{datadir} -type f -exec chmod 644 {} \;
	end
end

packages
	package %{name}
		requires
			libxslt
			docbook-dtds
			sgml-common
		end

		prerequires
			docbook-dtds
			libxml2
			sgml-common
		end

		script postin
			# Install catalog files.
			/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
				"http://docbook.sourceforge.net/release/xsl/%{version}" \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog

			/usr/bin/xmlcatalog --noout --add "rewriteURI" \
				"http://docbook.sourceforge.net/release/xsl/%{version}" \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog

			/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
				"http://docbook.sourceforge.net/release/xsl/current" \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog

			/usr/bin/xmlcatalog --noout --add "rewriteURI" \
				"http://docbook.sourceforge.net/release/xsl/current" \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog

			/usr/bin/xmlcatalog --noout --add "rewriteSystem" \
				"http://docbook.sourceforge.net/release/xsl-ns/current" \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog

			/usr/bin/xmlcatalog --noout --add "rewriteURI" \
				"http://docbook.sourceforge.net/release/xsl-ns/current" \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog
		end

		script preun
			# Uninstall catalog files.
			/usr/bin/xmlcatalog --noout --del \
				"file:///usr/share/sgml/docbook/xsl-ns-stylesheets-%{version}" /etc/xml/catalog
		end
	end
end
