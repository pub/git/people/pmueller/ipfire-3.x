###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = pakfire
version    = 0.9.29
release    = 1

maintainer = Michael Tremer <michael.tremer@ipfire.org>
groups     = System/Packaging
url        = https://git.ipfire.org/?p=pakfire.git;a=summary
license    = GPLv3+
summary    = Package installer/updater

description
	Pakfire optains package lists from the mirrors and can install and
	update packages.
end

source_dl  = https://source.ipfire.org/releases/pakfire/

build
	requires
		asciidoc
		autoconf
		automake
		bison
		curl-devel
		file-devel
		flex
		gettext-devel
		gpgme-devel
		intltool >= 0.40.0
		json-c-devel >= 0.15
		libarchive-devel >= 3.4.0
		libbpf-devel
		libcap-devel
		libmount-devel
		libnl3-devel
		libseccomp-devel
		libsolv-devel >= 0.7.5
		libuuid-devel
		openssl-devel >= 1.1.1
		pcre2-devel
		python3-cpuinfo
		python3-devel >= 3.6
		python3-kerberos
		python3-psutil
		python3-setproctitle
		python3-systemd
		python3-tornado
		sqlite-devel
		systemd-devel
		xz-devel
		zstd-devel
	end

	prepare_cmds
		# Generate the build system, if this was not a release
		# tarball.
		[ -x "configure" ] || sh ./autogen.sh
	end

	configure_options += \
		--enable-debug

	test
		LD_LIBRARY_PATH=%{DIR_APP}/src/.libs make check || true
	end

	install_cmds
		# Remove all example repository definitions.
		# Those will be solely provided by system-release.
		rm -rfv %{BUILDROOT}%{sysconfdir}/pakfire/repos/*
	end
end

packages
	package %{name}
		requires
			python3-cpuinfo
			python3-kerberos
			python3-psutil
			python3-setproctitle
			python3-systemd
			python3-tornado

			system-release
		end

		# Install this package into the build environment by default
		supplements
			build-essential
		end

		configfiles = %{sysconfdir}/pakfire/general.conf
	end

	package %{name}-builder
		summary = The Pakfire builder
		description
			The tools that are needed to build new pakfire packages.
		end

		requires
			pakfire = %{thisver}
		end

		configfiles
			%{sysconfdir}/pakfire/builder.conf
			%{sysconfdir}/pakfire/distros
		end

		files
			%{sysconfdir}/pakfire/builder.conf
			%{sysconfdir}/pakfire/distros
			%{bindir}/pakfire-builder
			%{mandir}/man*/pakfire-builder*
		end
	end

	package %{name}-client
		summary = The Pakfire Hub client
		description
			The pakfire client has the power to create build jobs
			and communicate with the Pakfire Build Service.
		end

		requires
			pakfire = %{thisver}
		end

		files
			%{sysconfdir}/pakfire/client.conf
			%{bindir}/pakfire-client
		end
		configfiles = %{sysconfdir}/pakfire/client.conf
	end

	package %{name}-daemon
		summary = The Pakfire daemon.
		description
			The Pakfire daemon takes build jobs from the
			Pakfire Build Service and compiles them.
		end

		requires
			pakfire-builder = %{thisver}
		end

		files
			%{sysconfdir}/pakfire/daemon.conf
			%{bindir}/pakfire-daemon
		end
		configfiles = %{sysconfdir}/pakfire/daemon.conf

		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script preun
			systemctl --no-reload disable pakfire-daemon.service > /dev/null 2>&1 || :
			systemctl stop pakfire-daemon.service > /dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload 2>&1 || :
			systemctl reload-or-try-restart pakfire-daemon.service >/dev/null 2>&1 || :
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
