###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = p11-kit
version    = 0.25.0
release    = 1

groups     = System/Libraries
url        = https://p11-glue.github.io/p11-glue/p11-kit.html
license    = BSD
summary    = Provides a way to load and enumerate PKCS#11 modules

description
	Provides a way to load and enumerate PKCS#11 modules. Provides a standard
	configuration setup for installing PKCS#11 modules in such a way that they're
	discoverable.

	Also solves problems with coordinating the use of PKCS#11 by different
	components or libraries living in the same process.
end

source_dl  = https://github.com/p11-glue/p11-kit/releases/download/%{version}/
sources    = %{thisapp}.tar.xz

build
	requires
		bash-completion-devel
		libffi-devel >= 3.0.0
		libtasn1-devel >= 2.3
		meson
		ninja
		systemd-devel
	end

	build
		%{meson} \
			-D trust_paths=/etc/pki/ca-trust/source
		%{meson_build}
	end

	install
		%{meson_install}
	end
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
