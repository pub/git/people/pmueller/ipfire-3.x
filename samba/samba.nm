###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = samba
version    = 4.19.0
release    = 1

groups     = Networking/Daemons
url        = https://www.samba.org/
license    = GPLv3+ and LGPLv3+
summary    = Server and Client software to interoperate with Windows machines.

description
	Samba is the suite of programs by which a lot of PC-related machines
	share files, printers, and other information (such as lists of
	available files and printers). The Windows NT, OS/2, and Linux
	operating systems support this natively, and add-on packages can
	enable the same thing for DOS, Windows, VMS, UNIX of all kinds, MVS,
	and more. This package provides an SMB/CIFS server that can be used to
	provide network services to SMB/CIFS clients.
	Samba uses NetBIOS over TCP/IP (NetBT) protocols and does NOT
	need the NetBEUI (Microsoft Raw NetBIOS frame) protocol.
end

source_dl  = https://www.samba.org/samba/ftp/stable/

CFLAGS    += \
	-D_FILE_OFFSET_BITS=64 \
	-D_GNU_SOURCE -DLDAP_DEPRECATED

build
	requires
		/usr/bin/rpcgen
		autoconf
		automake
		bison
		avahi-devel
		cups-devel >= 2.1.4
		dbus-devel
		docbook-xsl
		flex
		gettext
		gnutls-devel >= 3.7.8
		gpgme-devel
		jansson-devel
		ncurses-devel
		libacl-devel
		libarchive-devel
		libattr-devel
		libcap-devel
		libldb-devel >= 2.8.0
		libtalloc-devel >= 2.4.1
		libtdb-devel >= 1.4.9
		libtevent-devel >= 0.15.0
		libxcrypt-devel
		openldap-devel
		openssl-devel
		pam-devel
		perl(base)
		perl(FindBin)
		perl(JSON)
		perl(JSON::PP)
		perl(Parse::Yapp::Driver) >= 1.21
		popt-devel
		python3-devel
		python3-dns
		python3-markdown
		python3-ldb
		python3-talloc
		python3-tdb
		python3-tevent
		readline-devel
		which
		zlib-devel
	end

	configure_options += \
		--enable-fhs \
		--prefix=%{prefix} \
		--localstatedir=%{localstatedir} \
		--with-lockdir=%{sharedstatedir}/samba \
		--with-piddir=%{localstatedir}/run/samba \
		--with-privatedir=%{sharedstatedir}/samba/private \
		--with-logfilebase=%{localstatedir}/log/samba \
		--with-modulesdir=%{libdir}/samba \
		--with-configdir=%{sysconfdir}/samba \
		--with-pammodulesdir=%{libdir}/security \
		--with-automount \
		--with-pam \
		--with-quotas \
		--with-sendfile-support \
		--with-syslog \
		--with-utmp \
		--with-winbind \
		--bundled-libraries="!zlib,!popt,!tdb,!pytdb,!talloc,!pytalloc,!pytalloc-util,!tevent,!pytevent,!ldb,!pyldb,!pyldb-util" \
		--with-shared-modules=idmap_ad,idmap_rid,idmap_adex,idmap_hash

	install_cmds
		mkdir -pv %{BUILDROOT}%{sysconfdir}/samba
		echo "127.0.0.1 localhost" > %{BUILDROOT}%{sysconfdir}/samba/lmhosts
		cp -vf %{DIR_SOURCE}/smb.conf %{BUILDROOT}/%{sysconfdir}/%{name}

		# Drop /var/run
		rm -rvf %{BUILDROOT}%{localstatedir}/run
	end
end

packages
	package %{name}
		prerequires = systemd-units
		configfiles = /etc/smb.conf
		requires += %{name}-libs = %{thisver}
	end

	package %{name}-libs
		template LIBS
	end

	package %{name}-devel
		template DEVEL

		requires += %{name}-libs = %{thisver}
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
