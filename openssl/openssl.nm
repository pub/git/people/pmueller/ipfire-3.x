###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = openssl
version    = 3.1.2
release    = 2

maintainer = Michael Tremer <michael.tremer@ipfire.org>
groups     = System/Libraries
url        = https://www.openssl.org/
license    = OpenSSL
summary    = A general purpose cryptography library with TLS implementation.

description
	The OpenSSL toolkit provides support for secure communications between
	machines. OpenSSL includes a certificate management tool and shared
	libraries which provide various cryptographic algorithms and protocols.
end

source_dl  = https://openssl.org/source/

build
	requires
		ca-certificates
		coreutils
		perl
		perl(FindBin)
		perl(File::Compare)
		perl(File::Copy)
		perl(IPC::Cmd)
		perl(Math::BigInt)
		perl(Module::Load::Conditional)
		perl(Pod::Html)
		perl(Test::Harness)
		perl(Test::More)
		perl(Time::Local)
		sed
		zlib-devel
	end

	export HASHBANGPERL = %{bindir}/perl

	CFLAGS += -DPURIFY -Wa,--noexecstack

	# Set default ssl_arch.
	ssl_arch = linux-%{DISTRO_ARCH}

	if "%{DISTRO_ARCH}" == "x86_64"
		ssl_arch += enable-ec_nistp_64_gcc_128
	end

	if "%{DISTRO_ARCH}" == "aarch64"
		ssl_arch += enable-ec_nistp_64_gcc_128
	end

	configure = \
		./Configure \
			--prefix=%{prefix} \
			--libdir=%{libdir} \
			--openssldir=%{sysconfdir}/pki/tls \
			shared \
			zlib \
			enable-camellia \
			enable-seed \
			enable-rfc3779 \
			enable-ktls \
			no-rc4 \
			no-rc5 \
			no-sm2 \
			no-sm4 \
			%{ssl_arch} \
			${CFLAGS} \
			${LDFLAGS}

	test
		make test
	end

	install
		# Clean up the .pc files
		for i in libcrypto.pc libssl.pc openssl.pc; do
			sed -i '/^Libs.private:/{s/-L[^ ]* //;s/-Wl[^ ]* //}' $i
		done

		make install DESTDIR=%{BUILDROOT}

		# Remove dist config
		rm -vf %{BUILDROOT}%{sysconfdir}/pki/tls/openssl.cnf.dist
	end
end

packages
	package %{name}
		requires += %{name}-libs = %{thisver}
	end

	package %{name}-libs
		template LIBS

		requires += ca-certificates

		files += %{libdir}/openssl %{libdir}/engines*
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
