###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = smartmontools
version    = 7.4
release    = 1

groups     = System/Statistics
url        = https://www.smartmontools.org
license    = GPLv2
summary    = A set of tools that watches HDD S.M.A.R.T status

description
	The smartmontools package contains two utility programs (smartctl
	and smartd) to control and monitor storage systems using the
	Self-Monitoring, Analysis and Reporting Technology System (SMART)
	built into most modern ATA and SCSI harddisks. In many cases,
	these utilities will provide advanced warning of disk degradation
	and failure.
end

source_dl  = https://sourceforge.net/projects/smartmontools/files/smartmontools/%{version}

build
	requires
		gcc-c++
		libcap-ng-devel
		systemd-devel
	end

	configure_options += \
		--sysconfdir=%{sysconfdir}/%{name} \
		--with-libcap-ng=yes \
		--with-libsystemd \
		--with-systemdsystemunitdir=%{unitdir}

	test
		make check
	end
end

packages
	package %{name}
		configfiles
			%{sysconfdir}/smartmontools/smartd.conf
		end

		prerequires
			systemd-units
		end

		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script preun
			systemctl --no-reload disable smartd.service >/dev/null 2>&1 || :
			systemctl stop smartd.service >/dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload >/dev/null 2>&1 || :
			systemctl try-restart smartd.service >/dev/null 2>&1 || :
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
