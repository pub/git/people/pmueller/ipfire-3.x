###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = popt
version    = 1.19
release    = 1.1

groups     = System/Libraries
url        = https://github.com/rpm-software-management/popt
license    = MIT
summary    = C library for parsing command line parameters.

description
	Popt is a C library for parsing command line parameters. Popt was
	heavily influenced by the getopt() and getopt_long() functions, but
	it improves on them by allowing more powerful argument expansion.
end

source_dl  = https://github.com/rpm-software-management/popt/archive/refs/tags/
sources    = %{thisapp}-release.tar.gz

build
	DIR_APP = %{DIR_SRC}/popt-popt-%{version}-release

	requires
		autoconf
		automake
		gettext-devel
	end

	prepare_cmds
		./autogen.sh
	end
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
