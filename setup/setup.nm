###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = setup
version    = 3.0
release    = 19

groups     = Base Build System/Base
url        =
license    = Public Domain
summary    = A set of system configuration and setup files.

description
	The setup package contains a set of important system configuration and
	setup files, such as passwd, group, and profile.
end

# No tarball.
sources    =

build
	arches = noarch

	DIR_APP = %{DIR_SOURCE}

	requires
		perl
	end

	build
		bash ./shadowconvert.sh
	end

	test
		make check
	end

	install
		# Create missing folders.
		mkdir -pv %{BUILDROOT}/etc/profile.d %{BUILDROOT}/var/log

		# Install our config files.
		for i in aliases bashrc ethertypes exports filesystems group gshadow \
			host.conf hosts hosts.allow hosts.deny inputrc motd passwd \
			printcap profile protocols securetty services shadow shells; do \
			cp -vf %{DIR_APP}/${i} %{BUILDROOT}/etc || exit 1; \
		done

		# Create envvar overwrite file for bash.
		echo "#Add any required envvar overrides to this file, \
			it is sourced from /etc/profile" > %{BUILDROOT}%{sysconfdir}/profile.d/sh.local

		touch %{BUILDROOT}%{sysconfdir}/fstab
		chmod -v 0400 %{BUILDROOT}%{sysconfdir}/{,g}shadow

		# Create empty environment config file.
		touch %{BUILDROOT}%{sysconfdir}/environment
		chmod 0644 %{BUILDROOT}%{sysconfdir}/environment

		# Create MOTD (message of the day) folder layout.	
		mkdir -p %{BUILDROOT}/etc/motd.d
		mkdir -p %{BUILDROOT}/usr/lib/motd.d
		touch %{BUILDROOT}/usr/lib/motd
	
		# Create a tmpfiles file, needed for files in /run
		mkdir -p %{BUILDROOT}%{tmpfilesdir}	
		echo "f /run/motd 0644 root root -" > %{BUILDROOT}%{tmpfilesdir}/%{name}.conf
		echo "d /run/motd.d 0755 root root -" >> %{BUILDROOT}%{tmpfilesdir}/%{name}.conf
		chmod 0644 %{BUILDROOT}%{tmpfilesdir}/%{name}.conf

		mkdir -pv %{BUILDROOT}%{sysconfdir}/sysctl.d
		install -m 644 %{DIR_APP}/sysctl/printk.conf \
			%{BUILDROOT}%{sysconfdir}/sysctl.d/printk.conf
		install -m 644 %{DIR_APP}/sysctl/swappiness.conf \
			%{BUILDROOT}%{sysconfdir}/sysctl.d/swappiness.conf
		install -m 644 %{DIR_APP}/sysctl/kernel-hardening.conf \
			%{BUILDROOT}%{sysconfdir}/sysctl.d/kernel-hardening.conf
	end
end

packages
	package %{name}
		conflicts
			filesystem < 002
		end

		obsoletes
			iana-etc <= 2.30
		end

		provides
			iana-etc = 2.31
		end

		configfiles
			/etc/aliases
			/etc/environment
			/etc/ethertypes
			/etc/exports
			/etc/fstab
			/etc/group
			/etc/gshadow
			/etc/host.conf
			/etc/hosts
			/etc/hosts.allow
			/etc/hosts.deny
			/etc/motd
			/etc/passwd
			/etc/printcap
			/etc/protocols
			/etc/services
			/etc/shadow
			/etc/shells
		end
	end
end
