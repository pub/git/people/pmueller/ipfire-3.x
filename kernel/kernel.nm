###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = kernel
version    = 6.5.3
release    = 5
thisapp    = linux-%{version}

maintainer = Arne Fitzenreiter <arne.fitzenreiter@ipfire.org>
groups     = System/Kernels
url        = https://www.kernel.org/
license    = GPLv2
summary    = The Linux kernel.

description
	The kernel package contains the Linux kernel (vmlinuz), the core of any
	Linux operating system. The kernel handles the basic functions
	of the operating system: memory allocation, process allocation, device
	input and output, etc.
end

# Version helper variables.
localversion = %{release}.%{DISTRO_DISTTAG}.%{DISTRO_ARCH}
fullver      = %{version}-%{localversion}

source_dl    = https://www.kernel.org/pub/linux/kernel/v6.x/
sources      = %{thisapp}.tar.xz

build
	requires
		asciidoc
		bc
		binutils >= 2.25
		binutils-devel
		bison
		elfutils-devel
		flex
		gcc >= 8
		gcc-plugin-devel
		gettext
		hostname
		libunwind-devel
		ncurses-devel
		newt-devel
		openssl
		/usr/bin/openssl
		pciutils-devel
		perl
		python3-devel
		rsync
		openssl-devel
		slang-devel
		xmlto
		xz
		zstd

		/sbin/depmod
	end

	kernel_arch = %{DISTRO_PLATFORM}
	kernel_target = bzImage
	kernel_image = arch/%{kernel_arch}/boot/%{kernel_target}

	if "%{DISTRO_ARCH}" == "aarch64"
		kernel_arch = arm64
		kernel_target = Image.gz
	end

	prepare_cmds
		# Disable the colorize plugin.
		# It's generally very nice but it clutters our
		# logfiles.
		sed -e "/^COLORIZE_PLUGIN_CFLAGS/d" \
			-i Makefile

		# Use our linker flags while building the compiler plugins.
		sed -i '/^plugin_ldflags\s\+=/ s/$/ %{LDFLAGS}/' \
			scripts/gcc-plugins/Makefile

		# Remove Python 2 interpreter from a few scripts
		sed -e "s@^#!.*python@#!%{python3}@" \
			-i scripts/show_delta \
			-i scripts/tracing/draw_functrace.py

		# Get rid of unwanted files resulting from patch fuzz.
		find . \( -name "*.orig" -o -name "*~" \) -exec rm -f {} \; >/dev/null

		# Remove unnecessary SCM files.
		find . -name .gitignore -exec rm -f {} \; >/dev/null

		# This prevents scripts/setlocalversion from mucking with our version numbers.
		touch .scmversion

		mkdir -pv configs

		cd %{DIR_SOURCE}
		configure_kernel() {
			local flavour=${1}
			local suffix
			if [ "${flavour}" != "generic" ]; then
				suffix="-${flavour}"
			fi

			bash scripts/configure --kernel-dir=%{DIR_APP} \
				merge %{DISTRO_ARCH} ${flavour} %{DIR_APP}/configs/config.${flavour}
		}

		# Generate configuration files for all kernels we are going to
		# build.
		configure_kernel "generic"
	end

	build
		build_kernel() {
			local flavour=${1}
			local suffix
			local localversion="%{localversion}"
			local fullversion="%{fullver}"
			if [ "${flavour}" != "generic" ]; then
				suffix="-${flavour}"
				localversion="${localversion}.${flavour}"
				fullversion="${fullversion}.${flavour}"
			fi

			# Label the build.
			sed -i Makefile \
				-e "s/^EXTRAVERSION.*/EXTRAVERSION = -${localversion}/"

			# Clean up the source tree.
			make mrproper

			# Install configuration file.
			cp configs/config.${flavour} .config
			cat .config

			# Run the build.
			make ARCH=%{kernel_arch} HOSTCFLAGS="%{CFLAGS}" \
				HOSTLDFLAGS="%{LDFLAGS}" olddefconfig >/dev/null

			make ARCH=%{kernel_arch} %{kernel_target} modules \
				KERNELRELEASE=${fullversion} HOSTCFLAGS="%{CFLAGS}" \
				HOSTLDFLAGS="%{LDFLAGS}" %{PARALLELISMFLAGS}

			# Compile Flattened Device Tree database
			case "${DISTRO_ARCH}" in
				aarch64)
					make ARCH=%{kernel_arch} dtbs

					mkdir -pv %{BUILDROOT}/boot/dtb-${fullversion}
					for file in arch/arm/boot/dts/*.dtb; do
						[ -e "${file}" ] || continue

						install -m 644 ${file} %{BUILDROOT}/boot/dtb-${fullversion} || exit 1
						rm -f ${file}
					done
					;;
			esac

			# Install the results.
			mkdir -pv %{BUILDROOT}/boot %{BUILDROOT}/usr/lib/modules/${fullversion}
			install -m 755 %{kernel_image} %{BUILDROOT}/boot/vmlinuz-${fullversion}
			install -m 644 .config %{BUILDROOT}/boot/config-${fullversion}
			install -m 644 System.map %{BUILDROOT}/boot/System.map-${fullversion}

			# Create empty initramfs (70M).
			dd if=/dev/zero of=%{BUILDROOT}/boot/initramfs-${fullversion}.img bs=1M count=70

			# Install modules.
			make ARCH=%{kernel_arch} INSTALL_MOD_PATH=%{BUILDROOT}/usr mod-fw= V=1 \
				KERNELRELEASE=${fullversion} modules_install

			# And now the scary looking part.
			#
			# Recreate source and build links.
			rm -f %{BUILDROOT}/usr/lib/modules/${fullversion}/{build,source}
			mkdir -p %{BUILDROOT}/usr/lib/modules/${fullversion}/build
			ln -sf build %{BUILDROOT}/usr/lib/modules/${fullversion}/source

			# Create dirs for extra modules.
			mkdir -p %{BUILDROOT}/usr/lib/modules/${fullversion}/extra

			cp --parents $(find -type f -name "Makefile*" -o -name "Kconfig*") \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build
			cp Module.symvers System.map \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build
			rm -rf %{BUILDROOT}/usr/lib/modules/${fullversion}/build/{Documentation,scripts,include}

			cp .config %{BUILDROOT}/usr/lib/modules/${fullversion}/build
			cp -a scripts %{BUILDROOT}/usr/lib/modules/${fullversion}/build
			find %{BUILDROOT}/usr/lib/modules/${fullversion}/build/scripts -name "*.o" \
				-exec rm -vf {} \;

			cp -a --parents arch/%{kernel_arch}/include \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build
			cp -a include %{BUILDROOT}/usr/lib/modules/${fullversion}/build/include

			# Make sure we can build external modules.
			touch -r %{BUILDROOT}/usr/lib/modules/${fullversion}/build/Makefile \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build/include/linux/version.h
			touch -r %{BUILDROOT}/usr/lib/modules/${fullversion}/build/.config \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build/autoconf.h
			cp %{BUILDROOT}/usr/lib/modules/${fullversion}/build/.config \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build/include/config/auto.conf

			find %{BUILDROOT}/usr/lib/modules/${fullversion} -name "*.ko" -type f | \
				xargs --no-run-if-empty chmod u+x

			# Move all development files to /usr/src.
			mkdir -p %{BUILDROOT}/usr/src/kernels
			mv %{BUILDROOT}/usr/lib/modules/${fullversion}/build \
				%{BUILDROOT}/usr/src/kernels/${fullversion}
			ln -sf ../../../src/kernels/${fullversion} \
				%{BUILDROOT}/usr/lib/modules/${fullversion}/build

			# Fix permissions.
			find %{BUILDROOT}/usr/src/kernels -name "modules.order" \
				-exec chmod 644 {} \;

			find %{BUILDROOT}/usr/src/kernels -name ".*.cmd" -exec rm -f {} \;

			# remove device tree compiler because there are dead links
			rm -rf %{BUILDROOT}/usr/src/kernels/*/scripts/dtc
		}

		# Build the kernel
		build_kernel "generic"

		# cpupower
		chmod +x tools/power/cpupower/utils/version-gen.sh
		make -C tools/power/cpupower CPUFREQ_BENCH=false %{PARALLELISMFLAGS}

		pushd tools/thermal/tmon
		make
		popd
	end

	install
		# When no kernels are configured, we create a default config
		# which enables us to install at least the header files.
		[ -n "%{kernels}" ] || make ARCH=%{kernel_arch} defconfig

		# Install the header files
		make ARCH=%{kernel_arch} INSTALL_HDR_PATH=dest headers_install
		mkdir -pv %{BUILDROOT}/usr/include
		find dest/include \( -name .install -o -name ..install.cmd \) -delete
		cp -rv dest/include/* %{BUILDROOT}/usr/include

		# Glibc provides these headers
		rm -rfv %{BUILDROOT}/usr/include/scsi \
			%{BUILDROOT}/usr/include/asm*/atomic.h \
			%{BUILDROOT}/usr/include/asm*/io.h \
			%{BUILDROOT}/usr/include/asm*/irq.h

		# Remove all firmware files which are distributed by linux-firmware
		rm -rfv %{BUILDROOT}/lib/firmware

		# Install tools.
		# cpupower
		make -C tools/power/cpupower DESTDIR=%{BUILDROOT} libdir=%{libdir} \
			mandir=%{mandir} CPUFREQ_BENCH=false install

		chmod 0755 %{BUILDROOT}%{libdir}/libcpupower.so*

		# tmon
		pushd tools/thermal/tmon
		make INSTALL_ROOT=%{BUILDROOT} install
		popd
	end
end

packages
	package kernel-tools
		summary = Assortment of tools for the Linux kernel.
		description = %{summary}

		filter_requires
			perl>=[0-9]\:.*
		end
	end

	package kernel-tools-debuginfo
		template DEBUGINFO
	end

	package kernel
		summary = The Linux kernel %{fullver}

		prerequires
			/sbin/depmod
		end

		provides
			kernel = %{fullver}

			# Used to lock the running kernel from being uninstalled
			kernel(%{fullver})
		end

		requires
			dracut
			kernel-updater
			linux-firmware
			wireless-regdb

			# Install intel-microcode only on x86_64
			(intel-microcode if arch(x86_64))
		end

		files
			/boot/System.map-%{fullver}
			/boot/dtb-%{fullver}
			/boot/vmlinuz-%{fullver}
			/boot/initramfs-%{fullver}.img
			/usr/lib/modules/%{fullver}
		end

		_posttrans
			kernel-updater install %{fullver}
		end

		script posttransin
			%{_posttrans}
		end

		script posttransup
			%{_posttrans}
		end

		script postun
			kernel-updater uninstall %{fullver}
		end
	end

	package kernel-devel
		summary = Development files of kernel-%{fullver}
		description
			%{summary}

			Install this package in order to compile modules for
			kernel release %{fullver}.
		end

		provides
			kernel-devel = %{thisver}
		end

		requires
			kernel = %{thisver}
		end

		filter_requires
			perl>=[0-9]\:.*
		end

		files
			/usr/lib/modules/%{fullver}/build
			/usr/lib/modules/%{fullver}/source
			/usr/src/kernels/%{fullver}
		end
	end

	package kernel-headers
		summary = Header files of the kernel release %{fullver}
		desciption = %{summary}

		files
			/usr/include
		end
	end
end
