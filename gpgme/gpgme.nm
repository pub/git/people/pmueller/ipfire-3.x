###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = gpgme
version    = 1.18.0
release    = 2.1

groups     = Applications/System
url        = https://www.gnupg.org/related_software/gpgme/
license    = LGPLv2+
summary    = GnuPG Made Easy - high level crypto API

description
	GnuPG Made Easy (GPGME) is a library designed to make access to GnuPG
	easier for applications.  It provides a high-level crypto API for
	encryption, decryption, signing, signature verification and key
	management.
end

source_dl  = https://www.gnupg.org/ftp/gcrypt/gpgme/
sources    = %{thisapp}.tar.bz2

build
	requires
		gawk
		glib2-devel
		gnupg2
		libassuan-devel
		libgpg-error-devel
		pkg-config
		which
	end

	configure_options += \
		--disable-gpg-test

	# Tests hang indefinitely
	#test
	#	make -C tests check
	#end

	install_cmds
		# Remove LISP examples.
		rm -rfv %{BUILDROOT}%{datadir}/common-lisp/
	end
end

packages
	package %{name}
		requires = gnupg2
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
