###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = glibc
version    = 2.38
release    = 1

maintainer = Michael Tremer <michael.tremer@ipfire.org>
groups     = System/Base
url        = https://www.gnu.org/software/libc/libc.html
license    = GPLv2+ LGPLv2+
summary    = The GNU libc libraries.

description
	The glibc package contains standard libraries which are used by
	multiple programs on the system. In order to save disk space and
	memory, as well as to make upgrading easier, common system code is
	kept in one place and shared between programs. This particular package
	contains the most important sets of shared libraries: the standard C
	library and the standard math library. Without these two libraries, a
	Linux system will not function.
end

source_dl  = https://ftp.gnu.org/gnu/glibc/
sources    = %{thisapp}.tar.xz

build
	# Build in a separate directory
	DIR_BUILD = %{DIR_SRC}/glibc-build

	# Optimize glibc for kernel
	OPTIMIZED_KERNEL = 5.10

	requires
		autoconf
		automake
		binutils >= 2.35
		bison
		gcc >= 11
		gettext
		kernel-headers >= %{OPTIMIZED_KERNEL}
		libcap-devel
		libstdc++-static
		make >= 4.0
		patchelf
		python3 >= 3.4
		texinfo
	end

	# Disable LTO
	LTO_CFLAGS =

	# Path to put the runtime linker
	rtlddir = %{libdir}

	configure_options = \
		--build=%{DISTRO_BUILDTARGET} \
		--prefix=/usr \
		--libdir=%{libdir} \
		--libexecdir=%{libdir}/glibc \
		--sbindir=%{sbindir} \
		--enable-multi-arch \
		--enable-kernel=%{OPTIMIZED_KERNEL} \
		--enable-add-ons \
		--disable-werror \
		--disable-profile \
		--disable-crypt \
		--enable-fortify-source \
		--enable-stack-protector=all \
		--enable-bind-now \
		--with-bugurl=https://bugtracker.ipfire.org \
		--disable-build-nscd \
		--disable-nscd

	if "%{DISTRO_ARCH}" == "aarch64"
		configure_options += \
			--enable-memory-tagging

		# The runtime linker on aarch64 for whatever braindead reason is in /usr/lib
		rtlddir = %{prefix}/lib

		# Runtime Linker
		rtld = /lib/ld-linux-aarch64.so.1
	end

	if "%{DISTRO_ARCH}" == "x86_64"
		configure_options += \
			--enable-cet

		# Runtime Linker
		rtld = /lib64/ld-linux-x86-64.so.2
	end

	install
		# Install everything
		make install install_root=%{BUILDROOT} \
			rtlddir=%{rtlddir} rootsbindir=%{sbindir} slibdir=%{libdir}

		# Locales
		mkdir -pv %{BUILDROOT}/usr/lib/locale
		# This would install all locales that are supported
		make localedata/install-locales install_root=%{BUILDROOT}

		# Install runtime linker configuration
		install -v -m 644 %{DIR_SOURCE}/ld.so.conf %{BUILDROOT}%{sysconfdir}
		mkdir -pv %{BUILDROOT}%{sysconfdir}/ld.so.conf.d

		# Don't distribute linker cache
		rm -vf %{BUILDROOT}%{sysconfdir}/ld.so.cache

		# Install nsswitch.conf
		install -v -m 644 %{DIR_SOURCE}/nsswitch.conf %{BUILDROOT}%{sysconfdir}

		# Remove unused statically linked binaries
		rm -vf %{BUILDROOT}%{sbindir}/sln

		# Include /usr/lib/gconv/gconv-modules.cache
		> %{BUILDROOT}%{libdir}/gconv/gconv-modules.cache
		chmod 644 %{BUILDROOT}%{libdir}/gconv/gconv-modules.cache

		# Strip any object files
		strip --strip-debug %{BUILDROOT}%{libdir}/*.o

		# Some gconv libraries requires other libraries and contain $ORIGIN
		# as RUNPATH because they are all located in the same directory.
		# We do not support $ORIGIN as RUNPATH so change this to the directory
		# where the modules are located.
		find %{BUILDROOT}%{libdir}/gconv -type f -name "*.so" -exec sh -c '
			for file do
				# Obtain the RUNPATH of the module in case it has one set.
				runpath=$(patchelf --print-rpath ${file} | tr -d '\n')

				# Check if the RUNPATH is $ORIGIN
				if [ "${runpath}" == "\$ORIGIN" ]; then
					# Use patchelf to change the RUNPATH
					patchelf --set-rpath %{libdir}/gconv ${file}
				fi
			done ' sh {} +
	end
end

packages
	package glibc
		# Export the path to the runtime linker which is actually
		# installed into /usr, but is being searched for elsewhere
		provides += %{rtld}

		requires
			tzdata
		end

		conflicts
			kernel < %{OPTIMIZED_KERNEL}
		end

		obsoletes
			glibc-common < %{thisver}
		end
	end

	package glibc-devel
		template DEVEL

		requires
			glibc = %{thisver}
			glibc-headers = %{thisver}
			kernel-headers >= %{OPTIMIZED_KERNEL}
		end

		provides
			glibc-headers = %{thisver}
		end

		obsoletes
			glibc-headers < %{thisver}
		end

		files += %{libdir}/*.[ao]
	end

	package %{name}-utils
		summary = Development utilities from GNU C library
		description
			The glibc-utils package contains memusage, a memory usage profiler,
			mtrace, a memory leak tracer and xtrace, a function call tracer
			which can be helpful during program debugging.
		end

		files
			%{bindir}/memusage
			%{bindir}/memusagestat
			%{bindir}/mtrace
			%{bindir}/pcprofiledump
			%{bindir}/xtrace
			%{libdir}/libmemusage.so
			%{libdir}/libpcprofile.so
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
