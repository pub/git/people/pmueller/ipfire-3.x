From a25982ada523689c8745d7fb4b1b93c8f5dab2e7 Mon Sep 17 00:00:00 2001
From: "H.J. Lu" <hjl.tools@gmail.com>
Date: Thu, 14 Jul 2022 08:23:38 -0700
Subject: [PATCH] stack-protector: Check stack canary before throwing exception

Check stack canary before throwing exception to avoid stack corruption.

gcc/

	PR middle-end/58245
	* calls.cc: Include "tree-eh.h".
	(expand_call): Check stack canary before throwing exception.

gcc/testsuite/

	PR middle-end/58245
	* g++.dg/fstack-protector-strong.C: Adjusted.
	* g++.dg/pr58245-1.C: New test.
---
 gcc/calls.cc                                   |  6 +++++-
 gcc/testsuite/g++.dg/fstack-protector-strong.C |  2 +-
 gcc/testsuite/g++.dg/pr58245-1.C               | 10 ++++++++++
 3 files changed, 16 insertions(+), 2 deletions(-)
 create mode 100644 gcc/testsuite/g++.dg/pr58245-1.C

diff --git a/gcc/calls.cc b/gcc/calls.cc
index bc96aff38f0..6dd6f73e978 100644
--- a/gcc/calls.cc
+++ b/gcc/calls.cc
@@ -60,6 +60,7 @@ along with GCC; see the file COPYING3.  If not see
 #include "attr-fnspec.h"
 #include "value-query.h"
 #include "tree-pretty-print.h"
+#include "tree-eh.h"
 
 /* Like PREFERRED_STACK_BOUNDARY but in units of bytes, not bits.  */
 #define STACK_BYTES (PREFERRED_STACK_BOUNDARY / BITS_PER_UNIT)
@@ -3154,7 +3155,10 @@ expand_call (tree exp, rtx target, int ignore)
       if (pass && (flags & ECF_MALLOC))
 	start_sequence ();
 
-      if (pass == 0
+      /* Check the canary value for sibcall or function which doesn't
+	 return and could throw.  */
+      if ((pass == 0
+	   || ((flags & ECF_NORETURN) != 0 && tree_could_throw_p (exp)))
 	  && crtl->stack_protect_guard
 	  && targetm.stack_protect_runtime_enabled_p ())
 	stack_protect_epilogue ();
diff --git a/gcc/testsuite/g++.dg/fstack-protector-strong.C b/gcc/testsuite/g++.dg/fstack-protector-strong.C
index ae6d2fdb8df..034af2ce9ab 100644
--- a/gcc/testsuite/g++.dg/fstack-protector-strong.C
+++ b/gcc/testsuite/g++.dg/fstack-protector-strong.C
@@ -85,4 +85,4 @@ int foo7 (B *p)
   return p->return_slot ().a1;
 }
 
-/* { dg-final { scan-assembler-times "stack_chk_fail" 7 } } */
+/* { dg-final { scan-assembler-times "stack_chk_fail" 8 } } */
diff --git a/gcc/testsuite/g++.dg/pr58245-1.C b/gcc/testsuite/g++.dg/pr58245-1.C
new file mode 100644
index 00000000000..1439bc62e71
--- /dev/null
+++ b/gcc/testsuite/g++.dg/pr58245-1.C
@@ -0,0 +1,10 @@
+/* { dg-do compile { target i?86-*-* x86_64-*-* rs6000-*-* s390x-*-* } } */
+/* { dg-options "-O2 -fstack-protector-all" } */
+
+void
+bar (void)
+{
+  throw 1;
+}
+
+/* { dg-final { scan-assembler-times "stack_chk_fail" 1 } } */
-- 
2.31.1

