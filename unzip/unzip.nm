###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = unzip
version    = 6.0
release    = 6
thisapp    = %{name}60

groups     = Applications/Archiving
url        = https://infozip.sourceforge.net/UnZip.html
license    = BSD
summary    = A utility for unpacking zip files.

description
	The unzip utility is used to list, test, or extract files from a zip
	archive. Zip archives are commonly found on MS-DOS systems. The zip
	utility, included in the zip package, creates zip archives. Zip and
	unzip are both compatible with archives created by PKWARE(R)'s PKZIP
	for MS-DOS, but the programs' options and default behaviors do differ
	in some respects.
end


source_dl += https://downloads.sourceforge.net/infozip/

patches
	unzip-6.0-bzip2-configure.patch
	unzip-6.0-exec-shield.patch
	unzip-6.0-close.patch
	unzip-6.0-attribs-overflow.patch
	unzip-6.0-configure.patch
	unzip-6.0-manpage-fix.patch
	unzip-6.0-fix-recmatch.patch
	unzip-6.0-symlink.patch
	unzip-6.0-caseinsensitive.patch
	unzip-6.0-format-secure.patch
	unzip-6.0-valgrind.patch
	unzip-6.0-x-option.patch
	unzip-6.0-overflow.patch
	unzip-6.0-cve-2014-8139.patch
	unzip-6.0-cve-2014-8140.patch
	unzip-6.0-cve-2014-8141.patch
	unzip-6.0-overflow-long-fsize.patch
	unzip-6.0-heap-overflow-infloop.patch
	unzip-6.0-alt-iconv-utf8.patch
	unzip-6.0-alt-iconv-utf8-print.patch
	0001-Fix-CVE-2016-9844-rhbz-1404283.patch
	unzip-6.0-timestamp.patch
	unzip-6.0-cve-2018-1000035-heap-based-overflow.patch
	unzip-6.0-cve-2018-18384.patch
	unzip-6.0-COVSCAN-fix-unterminated-string.patch
	unzip-zipbomb-part1.patch
	unzip-zipbomb-part2.patch
	unzip-zipbomb-part3.patch
	unzip-zipbomb-manpage.patch
	unzip-zipbomb-part4.patch
	unzip-zipbomb-part5.patch
	unzip-zipbomb-part6.patch
	unzip-zipbomb-switch.patch
	unzip-gnu89-build.patch
end

CFLAGS    += -D_LARGEFILE64_SOURCE

build
	requires
		bzip2-devel
	end

	DIR_APP = %{DIR_SRC}/%{thisapp}

	build
		make -f unix/Makefile prefix=%{prefix} \
			CF="%{CFLAGS} -I." LF2="%{LDFLAGS}" unzips %{PARALLELISMFLAGS}
	end

	install
		make -f unix/Makefile prefix=%{BUILDROOT}%{prefix} \
			MANDIR=%{BUILDROOT}%{mandir}/man1 INSTALL="cp -p" install
	end
end

packages
	package %{name}

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
