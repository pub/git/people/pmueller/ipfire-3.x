###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = perl-IO-String
version    = 1.08
release    = 1
thisapp    = IO-String-%{version}

groups     = Development/Libraries
url        = http://search.cpan.org/dist/IO-String/
license    = Perl
summary    = Emulate file interface for in-core strings

description
	The IO::String module provides the IO::File interface for in-core
	strings. An IO::String object can be attached to a string, and makes
	it possible to use the normal file operations for reading or writing
	data, as well as for seeking to various locations of the string.
	This is useful when you want to use a library module that only provides
	an interface to file handles on data that you have in a string variable.
end

source_dl  = https://cpan.metacpan.org/authors/id/G/GA/GAAS/

build
	arches = noarch

	requires
		perl(ExtUtils::MakeMaker)
		perl(ExtUtils::ParseXS)
		perl(Test)
	end

	build
		perl Makefile.PL INSTALLDIRS=vendor OPTIMIZE="%{CFLAGS}"
		make %{PARALLELISMFLAGS}
	end

	test
		make test
	end

	make_install_targets = pure_install DESTDIR=%{BUILDROOT}
end

packages
	package %{name}
		requires
			perl
		end
	end
end
