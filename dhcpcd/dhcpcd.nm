###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = dhcpcd
version    = 10.0.2
release    = 1

groups     = Network/Base
url        = https://roy.marples.name/projects/dhcpcd/
license    = BSD
summary    = A DHCP and DHCPv6 client.

description
	dhcpcd is a DHCP and DHCPv6 client. It is currently the most feature-rich
	open source DHCP client.
end

source_dl  = https://github.com/NetworkConfiguration/dhcpcd/releases/download/v%{version}/
sources    = %{thisapp}.tar.xz

build
	configure_options += \
		--dbdir=%{sharedstatedir}/dhcpcd

	prepare_cmds
		%{create_user}
	end

	test
		make test
	end

	install_cmds
		# Assign db directory to the correct user and group.
		chown dhcpcd:dhcpcd %{BUILDROOT}%{sharedstatedir}/dhcpcd

		# Fix permission of /sbin/dhcpcd
		chmod 755 %{BUILDROOT}%{sbindir}/dhcpcd
	end
end

create_user
	getent group dhcpcd >/dev/null || groupadd -r dhcpcd
	getent passwd dhcpcd >/dev/null || useradd -r -g dhcpcd \
		-d /var/lib/dhcpcd -s /sbin/nologin -c "DHCP client user" dhcpcd
end

packages
	package %{name}
		prerequires
			shadow-utils
			systemd-units
		end

		configfiles
			%{sysconfdir}/dhcpcd.conf
		end

		datafiles
			%{sharedstatedir}/dhcpcd
		end

		script prein
			%{create_user}
		end

		script postin
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postun
			systemctl daemon-reload >/dev/null 2>&1 || :
		end

		script postup
			systemctl daemon-reload >/dev/null 2>&1 || :
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
